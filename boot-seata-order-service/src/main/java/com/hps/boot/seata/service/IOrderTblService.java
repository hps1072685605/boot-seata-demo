package com.hps.boot.seata.service;

import com.hps.boot.seata.entity.OrderTbl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author heps
 * @since 2020-09-28
 */
public interface IOrderTblService extends IService<OrderTbl> {

  void create(String userId, String commodityCode, Integer count);
}
