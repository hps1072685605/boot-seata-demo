package com.hps.boot.seata;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 *
 * @author heps
 * @since 2020/9/28
 */
@MapperScan("com.hps.boot.seata.mapper")
@SpringBootApplication(scanBasePackages = "com.hps.boot.seata", exclude = {DataSourceAutoConfiguration.class})
public class OrderServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(OrderServiceApplication.class, args);
  }
}
