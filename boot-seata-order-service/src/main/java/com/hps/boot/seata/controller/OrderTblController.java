package com.hps.boot.seata.controller;


import com.hps.boot.seata.service.IOrderTblService;
import io.seata.core.context.RootContext;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author heps
 * @since 2020-09-28
 */
@Slf4j
@RestController
@RequestMapping("/order")
public class OrderTblController {

  @Resource
  private IOrderTblService orderTblService;

  @GetMapping(value = "/debit")
  public void debit(@RequestParam String userId, @RequestParam String commodityCode, @RequestParam Integer count) {
    log.info("order XID " + RootContext.getXID());
    orderTblService.create(userId, commodityCode, count);
  }
}
