package com.hps.boot.seata.client;

import java.math.BigDecimal;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author heps
 * @since 2020/9/28
 */
@Slf4j
@Component
public class AccountClient {

  @Resource
  private RestTemplate restTemplate;

  public void debit(String userId, BigDecimal orderMoney) {
    String url = "http://127.0.0.1:9071/account?userId=" + userId + "&orderMoney=" + orderMoney;
    try {
      restTemplate.getForEntity(url, Void.class);
    } catch (Exception e) {
      log.error("debit url {} ,error:", url, e);
      throw new RuntimeException();
    }
  }
}
