package com.hps.boot.seata.service.impl;

import com.hps.boot.seata.client.AccountClient;
import com.hps.boot.seata.entity.OrderTbl;
import com.hps.boot.seata.mapper.OrderTblMapper;
import com.hps.boot.seata.service.IOrderTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.math.BigDecimal;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author heps
 * @since 2020-09-28
 */
@Service
public class OrderTblServiceImpl extends ServiceImpl<OrderTblMapper, OrderTbl> implements IOrderTblService {

  @Resource
  private AccountClient accountClient;

  @Override
  public void create(String userId, String commodityCode, Integer count) {
    BigDecimal orderMoney = new BigDecimal(count).multiply(new BigDecimal(5));
    OrderTbl order = new OrderTbl();
    order.setUserId(userId);
    order.setCommodityCode(commodityCode);
    order.setCount(count);
    order.setMoney(orderMoney);

    baseMapper.insert(order);

    accountClient.debit(userId, orderMoney);
  }
}
