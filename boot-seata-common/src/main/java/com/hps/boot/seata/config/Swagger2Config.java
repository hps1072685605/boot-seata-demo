package com.hps.boot.seata.config;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

/**
 *
 * @author heps
 * @since 2020/9/27
 */
@Configuration
@EnableSwagger2WebMvc
public class Swagger2Config {

}
