package com.hps.boot.seata.service;

import com.hps.boot.seata.entity.StorageTbl;
import com.baomidou.mybatisplus.extension.service.IService;
import java.sql.SQLException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author heps
 * @since 2020-09-28
 */
public interface IStorageTblService extends IService<StorageTbl> {

  void deduct(String commodityCode, int count);

  void batchUpdate() throws SQLException;

  void batchDelete() throws SQLException;
}
