package com.hps.boot.seata.mapper;

import com.hps.boot.seata.entity.StorageTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author heps
 * @since 2020-09-28
 */
public interface StorageTblMapper extends BaseMapper<StorageTbl> {

}
