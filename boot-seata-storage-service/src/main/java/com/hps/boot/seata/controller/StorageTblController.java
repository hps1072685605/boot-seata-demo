package com.hps.boot.seata.controller;


import com.hps.boot.seata.entity.StorageTbl;
import com.hps.boot.seata.service.IStorageTblService;
import io.seata.core.context.RootContext;
import java.sql.SQLException;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author heps
 * @since 2020-09-28
 */
@RestController
@RequestMapping("/storage")
public class StorageTblController {

  @Resource
  private IStorageTblService storageService;

  @GetMapping(value = "/deduct")
  public void deduct(@RequestParam String commodityCode, @RequestParam Integer count) {
    System.out.println("storage XID " + RootContext.getXID());
    storageService.deduct(commodityCode, count);
  }

  @GetMapping(value = "/get/{id}")
  public StorageTbl getById(@PathVariable("id") Integer id) {
    return storageService.getById(id);
  }

  @GetMapping(value = "/batch/update")
  public void batchUpdateCond() {
    try {
      storageService.batchUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @GetMapping(value = "/batch/delete")
  public void batchDeleteCond() {
    try {
      storageService.batchDelete();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
