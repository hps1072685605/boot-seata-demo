package com.hps.boot.seata.service;

/**
 *
 * @author heps
 * @since 2020/9/28
 */
public interface IBusinessService {

  void purchase(String userId, String commodityCode, int orderCount);
}
