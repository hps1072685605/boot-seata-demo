package com.hps.boot.seata.service.impl;

import com.hps.boot.seata.client.OrderClient;
import com.hps.boot.seata.client.StorageClient;
import com.hps.boot.seata.service.IBusinessService;
import io.seata.spring.annotation.GlobalTransactional;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 *
 * @author heps
 * @since 2020/9/28
 */
@Service
public class BusinessServiceImpl implements IBusinessService {

  @Resource
  private OrderClient orderClient;

  @Resource
  private StorageClient storageClient;

  @Override
  @GlobalTransactional
  public void purchase(String userId, String commodityCode, int orderCount) {
    storageClient.deduct(commodityCode, orderCount);
    orderClient.create(userId, commodityCode, orderCount);
  }
}
