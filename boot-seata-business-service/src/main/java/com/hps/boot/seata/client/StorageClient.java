package com.hps.boot.seata.client;

import io.seata.core.context.RootContext;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author heps
 * @since 2020/9/28
 */
@Slf4j
@Component
public class StorageClient {

  @Resource
  private RestTemplate restTemplate;

  public void deduct(String commodityCode, int orderCount) {
    System.out.println("business to storage " + RootContext.getXID());
    String url = "http://127.0.0.1:9073/storage/deduct?commodityCode=" + commodityCode + "&count=" + orderCount;
    try {
      restTemplate.getForEntity(url, Void.class);
    } catch (Exception e) {
      log.error("deduct url {} ,error:", url, e);
      throw new RuntimeException();
    }
  }
}
