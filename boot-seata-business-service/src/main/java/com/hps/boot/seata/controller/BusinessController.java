package com.hps.boot.seata.controller;

import com.hps.boot.seata.service.IBusinessService;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author heps
 * @since 2020/9/28
 */
@RestController
@RequestMapping("/business")
public class BusinessController {

  @Resource
  private IBusinessService businessService;

  /**
   * 购买下单，模拟全局事务提交
   *
   * @return
   */
  @RequestMapping("/purchase/commit")
  public Boolean purchaseCommit(HttpServletRequest request) {
    businessService.purchase("1001", "2001", 1);
    return true;
  }

  /**
   * 购买下单，模拟全局事务回滚
   *
   * @return
   */
  @RequestMapping("/purchase/rollback")
  public Boolean purchaseRollback() {
    try {
      businessService.purchase("1002", "2001", 1);
    } catch (Exception e) {
      e.printStackTrace();
      return false;
    }

    return true;
  }
}
