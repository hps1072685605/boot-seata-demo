package com.hps.boot.seata.client;

import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author heps
 * @since 2020/9/28
 */
@Slf4j
@Component
public class OrderClient {

  @Resource
  private RestTemplate restTemplate;

  public void create(String userId, String commodityCode, int orderCount) {
    String url = "http://127.0.0.1:9072/order/debit?userId=" + userId + "&commodityCode=" + commodityCode + "&count=" + orderCount;
    try {
      restTemplate.getForEntity(url, Void.class);
    } catch (Exception e) {
      log.error("create url {} ,error:", url);
      throw new RuntimeException();
    }
  }
}
