package com.hps.boot.seata.service;

import com.hps.boot.seata.entity.AccountTbl;
import com.baomidou.mybatisplus.extension.service.IService;
import java.math.BigDecimal;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author heps
 * @since 2020-09-27
 */
public interface IAccountTblService extends IService<AccountTbl> {

  void debit(String userId, BigDecimal num);
}
