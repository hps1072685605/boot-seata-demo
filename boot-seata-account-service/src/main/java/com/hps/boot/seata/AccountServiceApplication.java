package com.hps.boot.seata;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 *
 * @author heps
 * @since 2020/9/27
 */
@MapperScan("com.hps.boot.seata.mapper")
@SpringBootApplication(scanBasePackages = "com.hps.boot.seata", exclude = {DataSourceAutoConfiguration.class})
public class AccountServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(AccountServiceApplication.class, args);
  }
}
