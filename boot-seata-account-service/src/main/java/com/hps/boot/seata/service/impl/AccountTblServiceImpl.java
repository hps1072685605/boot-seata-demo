package com.hps.boot.seata.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hps.boot.seata.entity.AccountTbl;
import com.hps.boot.seata.mapper.AccountTblMapper;
import com.hps.boot.seata.service.IAccountTblService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.math.BigDecimal;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author heps
 * @since 2020-09-27
 */
@Service
public class AccountTblServiceImpl extends ServiceImpl<AccountTblMapper, AccountTbl> implements IAccountTblService {

  private static final String ERROR_USER_ID = "1002";


  @Override
  public void debit(String userId, BigDecimal num) {
    AccountTbl account = baseMapper.selectOne(new QueryWrapper<AccountTbl>()
        .lambda()
        .eq(AccountTbl::getUserId, userId)
    );
    account.setMoney(account.getMoney().subtract(num));
    baseMapper.updateById(account);

    if (ERROR_USER_ID.equals(userId)) {
      throw new RuntimeException("account branch exception");
    }
  }
}
