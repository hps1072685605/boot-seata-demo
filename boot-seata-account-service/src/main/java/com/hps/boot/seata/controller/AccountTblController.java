package com.hps.boot.seata.controller;


import com.hps.boot.seata.service.IAccountTblService;
import io.seata.core.context.RootContext;
import io.swagger.annotations.Api;
import java.math.BigDecimal;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author heps
 * @since 2020-09-27
 */
@Slf4j
@RestController
@RequestMapping("/account")
@Api(value = "账号信息", tags = "账号信息")
public class AccountTblController {

  @Resource
  private IAccountTblService accountTblService;

  @GetMapping
  public void debit(@RequestParam String userId, @RequestParam BigDecimal orderMoney) {
    log.info("account XID " + RootContext.getXID());
    accountTblService.debit(userId, orderMoney);
  }
}
