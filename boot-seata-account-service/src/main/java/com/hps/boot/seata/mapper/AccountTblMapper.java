package com.hps.boot.seata.mapper;

import com.hps.boot.seata.entity.AccountTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author heps
 * @since 2020-09-27
 */
public interface AccountTblMapper extends BaseMapper<AccountTbl> {

}
